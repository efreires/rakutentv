package com.eladiosuarez.rakuten.viewmodel

import android.util.ArrayMap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eladiosuarez.rakuten.MoviesApp
import com.eladiosuarez.rakuten.data.repository.MovieDataSource
import com.eladiosuarez.rakuten.ui.ActivityNavigator
import com.eladiosuarez.rakuten.ui.detailmovie.viewmodel.MoviesDetailViewModel
import com.eladiosuarez.rakuten.ui.movies.viewmodel.MoviesViewModel
import java.util.concurrent.Callable

class MovieViewModelFactory(
    movielDataSource: MovieDataSource,
    application: MoviesApp,
    activityNavigator: ActivityNavigator
) : ViewModelProvider.Factory {

    private val mCreators: ArrayMap<Class<*>, Callable<out ViewModel>> = ArrayMap()

    init {
        mCreators[MoviesViewModel::class.java] = Callable {
            MoviesViewModel(
                application = application,
                mMovieApiDataSource = movielDataSource,
                mActivityNavigator = activityNavigator
            )
        }

        mCreators[MoviesDetailViewModel::class.java] = Callable {
            MoviesDetailViewModel(
                application = application,
                mMovieApiDataSource = movielDataSource,
                mActivityNavigator = activityNavigator
            )
        }

    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator: Callable<out ViewModel>? = mCreators[modelClass]
        if (creator == null) {
            for ((key, value) in mCreators) {
                if (modelClass.isAssignableFrom(key)) {
                    creator = value
                    break
                }
            }
        }
        if (creator == null)
            throw IllegalArgumentException("Unknown model class $modelClass")

        try {
            return creator.call() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}