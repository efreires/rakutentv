package com.eladiosuarez.rakuten.ui.detailmovie.view

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.eladiosuarez.rakuten.R
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesDetailStreamingBindingModel
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesListBindingModel
import com.eladiosuarez.rakuten.data.model.request.streaming.StreamingRequest
import com.eladiosuarez.rakuten.databinding.ActivityMovieDetailBinding
import com.eladiosuarez.rakuten.ui.base.BaseActivity
import com.eladiosuarez.rakuten.ui.detailmovie.viewmodel.MoviesDetailViewModel
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_movie_detail.*
import javax.inject.Inject

/**
 * MovieDetailActivity
 * @author Eladio Freire
 */
class MovieDetailActivity : BaseActivity() {

    private lateinit var mMoviesListBindingModel: MoviesListBindingModel
    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory
    private var mViewModel: MoviesDetailViewModel? = null
    var mBinding: ActivityMovieDetailBinding? = null
    private lateinit var mBandwidthMeter: BandwidthMeter
    private lateinit var mVideoTrackSelectionFactory: AdaptiveTrackSelection.Factory
    private lateinit var mStreamingInfo: MoviesDetailStreamingBindingModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail)
        AndroidInjection.inject(this)
        mBandwidthMeter = DefaultBandwidthMeter()
        mVideoTrackSelectionFactory = AdaptiveTrackSelection.Factory(mBandwidthMeter)
        mMoviesListBindingModel = intent.getParcelableExtra(BUNDLE_MOVIE_ID)
        ViewModelProviders
            .of(this, mViewModelFactory)
            .get(MoviesDetailViewModel::class.java)
            .also {
                mViewModel = it
                observeViewModel()
            }
        mBinding?.setBackListener(this::onClickBack)
    }

    /**
     * Observe the viewModel. GetMovieDetailsList return the movie detail in a mutableLiveData.
     * When receive the infoMovie mapped, save this in a variable for use it when click in the trailer button.
     */
    private fun observeViewModel() {
        mViewModel?.apply {
            getMovieDetailList(mMoviesListBindingModel.id)
            getMutableMovieData().observeForever {
                mStreamingInfo = it.streamingPost
                mBinding?.run {
                    dataMovie = it
                    playTrailer.setOnClickListener {
                        getMovieTrailer(
                            streamingRequest = StreamingRequest(
                                mStreamingInfo.audioLanguage,
                                mStreamingInfo.audioQuality,
                                mStreamingInfo.contentId,
                                mStreamingInfo.contentType,
                                mStreamingInfo.deviceSerial,
                                mStreamingInfo.deviceStreamVideoQuality,
                                mStreamingInfo.player,
                                mStreamingInfo.subtitleLanguage,
                                mStreamingInfo.videoType
                            )
                        )
                    }
                }
            }
            getMutableLiveDataTrailer().observeForever {
                Log.d(TAG, "Trailer")
                onMovieTrailerSelected(it.streamInfos[0].url)
            }
            getMutableDataError().observeForever {
                Log.e(TAG, it)
                progres_detail.visibility = View.GONE

            }

            getMutableLoadingMovies().observeForever {
                Log.w(TAG, it.toString())
                progres_detail.visibility = View.GONE
            }
        }
    }

    /**
     * Click back finish activity.
     */
    fun onClickBack(view: View) {
        finishAndRemoveTask()

    }

    companion object {
        const val BUNDLE_MOVIE_ID = "movie_id"
        val TAG = MovieDetailActivity::class.java.canonicalName
    }
}
