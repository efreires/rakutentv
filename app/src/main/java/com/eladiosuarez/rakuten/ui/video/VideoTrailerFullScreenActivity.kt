package com.eladiosuarez.rakuten.ui.video

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.eladiosuarez.rakuten.R
import com.eladiosuarez.rakuten.databinding.ActivityVideoTrailerFullScreenBinding
import com.eladiosuarez.rakuten.ui.base.BaseActivity
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_video_trailer_full_screen.*

/**
 * VideoTrailerFullScreenActivity
 * @author Eladio Freire
 */
class VideoTrailerFullScreenActivity : BaseActivity() {
    private var trackSelector: DefaultTrackSelector? = null
    var binding: ActivityVideoTrailerFullScreenBinding? = null
    private var lastSeenTrackGroupArray: TrackGroupArray? = null
    private lateinit var videoTrackSelectionFactory: AdaptiveTrackSelection.Factory
    private var currentWindow: Int = 0
    private var playbackPosition: Long = 0
    private lateinit var player: SimpleExoPlayer
    private lateinit var mediaDataSourceFactory: DataSource.Factory
    private lateinit var bandwidthMeter: BandwidthMeter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video_trailer_full_screen)
        bandwidthMeter = DefaultBandwidthMeter()
        videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        binding?.setCloseTrailer(this::onTrailerClose)
        initializePlayer(intent.getStringExtra(BUNDLE_MOVIE_TRAILER_ID))
    }

    /**
     * Initialize player with the url that is sent from detail Activity.
     */
    private fun initializePlayer(url: String) {
        trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        mediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"))

        val mediaSource = ExtractorMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse(url))

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)

        with(player) {
            prepare(mediaSource, false, false)
            playWhenReady = true
        }


        playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        playerView.player = player
        playerView.requestFocus()

        lastSeenTrackGroupArray = null
    }


    private fun updateStartPosition() {

        with(player) {
            playbackPosition = currentPosition
            currentWindow = currentWindowIndex
            playWhenReady = playWhenReady
        }
    }

    private fun onTrailerClose(view: View) {
        finishAndRemoveTask()
        releasePlayer()
    }

    private fun releasePlayer() {
        updateStartPosition()
        player.release()
        trackSelector = null
    }

    public override fun onPause() {
        super.onPause()

        if (Util.SDK_INT <= 23) releasePlayer()
    }

    public override fun onStop() {
        super.onStop()

        if (Util.SDK_INT > 23) releasePlayer()
    }

    companion object {
        const val BUNDLE_MOVIE_TRAILER_ID = "movie_trailer_id"
    }
}
