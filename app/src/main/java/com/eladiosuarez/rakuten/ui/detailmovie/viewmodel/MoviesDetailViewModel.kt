package com.eladiosuarez.rakuten.ui.detailmovie.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesDetailBindingModel
import com.eladiosuarez.rakuten.data.model.request.streaming.StreamingRequest
import com.eladiosuarez.rakuten.data.model.response.streaming.DataTrailer
import com.eladiosuarez.rakuten.data.repository.MovieDataSource
import com.eladiosuarez.rakuten.ui.ActivityNavigator
import com.eladiosuarez.rakuten.ui.detailmovie.mapper.MovieDetailBindingModelMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

/**
 * MoviesDetailViewModel
 * @author Eladio Freire
 */
class MoviesDetailViewModel(
    application: Application,
    private val mMovieApiDataSource: MovieDataSource,
    private val mActivityNavigator: ActivityNavigator
) : AndroidViewModel(application) {

    private val mDisposableMovies = CompositeDisposable()
    private val mMutableLiveDataMovie by lazy { MutableLiveData<MoviesDetailBindingModel>() }
    private val mMutableLiveDataTrailer by lazy { MutableLiveData<DataTrailer>() }
    private val mMutableLiveDataLoading by lazy { MutableLiveData<Boolean>() }
    private val mMutableLiveDataError by lazy { MutableLiveData<String>() }

    /**
     * Get Movie Details. Call to the MovieApiDataSource to get the movie detail.
     * These return an Single observable that put inside disposable and get the events in the subscribeBy and send
     * the events in the MutableLiveData if is error, loading finish or value data.
     * @param id: Movie id.
     */
    fun getMovieDetailList(id: String) {

        mDisposableMovies.add(
            mMovieApiDataSource.getMovieDetail(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    MovieDetailBindingModelMapper.mapMovieToMovieBindingModel(it)
                }
                .toObservable()
                .subscribeBy(
                    onNext = {
                        mMutableLiveDataMovie.value = it
                    },
                    onError = {
                        mMutableLiveDataError.value = it.localizedMessage
                        mMutableLiveDataLoading.setValue(false)
                    },
                    onComplete = {
                        mMutableLiveDataLoading.setValue(true)
                    }
                ))
    }

    /**
     * Get Movie Trailee. Call to the MovieApiDataSource to get the movie trailer.
     * These return an Single observable that put inside disposable and get the events in the subscribeBy and send
     * the events in the MutableLiveData if is error, loading finish or value data.
     * @param streamingRequest: Object Streaming request.
     */
    fun getMovieTrailer(streamingRequest: StreamingRequest) {

        mDisposableMovies.add(
            mMovieApiDataSource.postTrailerStreaming(streamingRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .map {
                    it.mData
                }
                .subscribeBy(
                    onNext = {
                        mMutableLiveDataTrailer.value = it
                    },
                    onError = {
                        mMutableLiveDataError.value = it.localizedMessage
                        mMutableLiveDataLoading.setValue(false)
                    },
                    onComplete = {
                        mMutableLiveDataLoading.setValue(true)
                    }
                ))
    }

    /**
     * Get MutableLiveData to return the info movie to the ui.
     */
    public fun getMutableMovieData(): MutableLiveData<MoviesDetailBindingModel> {
        return mMutableLiveDataMovie
    }

    /**
     * Get MutableLiveData to return the info trailer to the ui.
     */
    fun getMutableLiveDataTrailer(): MutableLiveData<DataTrailer> {
        return mMutableLiveDataTrailer
    }

    /**
     * Get MutableLiveData to return the errors in the server request to the ui.
     */
    fun getMutableDataError(): MutableLiveData<String> {
        return mMutableLiveDataError
    }

    /**
     * Get MutableLiveData to return when the request has finished.
     */
    fun getMutableLoadingMovies(): MutableLiveData<Boolean> {

        return mMutableLiveDataLoading
    }

    /**
     * Navigate to the movie trailer.
     */
    fun onMovieTrailerSelected(url: String) {
        mActivityNavigator.openMovieTrailer(url = url)
    }

    override fun onCleared() {
        super.onCleared()
        mDisposableMovies.dispose()
    }
}