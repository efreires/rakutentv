package com.eladiosuarez.rakuten.ui.detailmovie.mapper

import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesDetailBindingModel
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesDetailStreamingBindingModel
import com.eladiosuarez.rakuten.data.model.response.detailmovie.DetailMovieResponse

/**
 * MovieDetailBindingModelMapper
 * @author Eladio Freire
 */
object MovieDetailBindingModelMapper {


    fun mapMovieToMovieBindingModel(
        movies: DetailMovieResponse
    ): MoviesDetailBindingModel {
        val moviesDetailStreamingBindingModel = MoviesDetailStreamingBindingModel(
            audioLanguage = movies.dataDetail.viewOptions.public.trailers[0].audioLanguages[0].id,
            audioQuality = movies.dataDetail.viewOptions.support.audioQualities[0].id,
            contentId = movies.dataDetail.id,
            contentType = movies.dataDetail.type,
            deviceSerial = "AABBCCDDCC112233",
            deviceStreamVideoQuality = movies.dataDetail.viewOptions.support.videoQualities[0].id,
            player = "android:PD-NONE",
            subtitleLanguage = movies.dataDetail.viewOptions.public.trailers[0].subtitleLanguages[0].id,
            videoQuality = movies.dataDetail.viewOptions.public.trailers[0].videoQualities[0].id,
            videoType = "trailer"
        )
        return MoviesDetailBindingModel(
            moviesDetailStreamingBindingModel,
            score = movies.dataDetail.scores,
            genre = movies.dataDetail.genres.map { it.id },
            synopsis = movies.dataDetail.shortPlot,
            actors = movies.dataDetail.actors.map { it.name },
            title = movies.dataDetail.title,
            originalTitle = movies.dataDetail.originalTitle,
            language = movies.dataDetail.viewOptions.private.streams[0].audioLanguages.map { it.name },
            country = movies.dataDetail.countries[0].name,
            duration = movies.dataDetail.duration.toString(),
            rent = movies.dataDetail.orderOptions[0].price,
            sell = movies.dataDetail.orderOptions.filter { it.purchaseType.name.contains("Venta") }[0].price,
            age = movies.dataDetail.classification.age.toString(),
            year = movies.dataDetail.year.toString(),
            poster = movies.dataDetail.images.artwork,
            snapshot = movies.dataDetail.images.snapshot
        )
    }
}