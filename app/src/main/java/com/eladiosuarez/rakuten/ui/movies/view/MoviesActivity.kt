package com.eladiosuarez.rakuten.ui.movies.view

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.eladiosuarez.rakuten.R
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesListBindingModel
import com.eladiosuarez.rakuten.ui.base.BaseActivity
import com.eladiosuarez.rakuten.ui.movies.adapter.MoviesListAdapter
import com.eladiosuarez.rakuten.ui.movies.adapter.MoviesTypesListAdapter
import com.eladiosuarez.rakuten.ui.movies.viewmodel.MoviesViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_movies.*
import javax.inject.Inject

/**
 * MoviesActivity
 * @author Eladio Freire
 */
class MoviesActivity : BaseActivity(), MoviesListAdapter.OnItemClickListener,
    MoviesTypesListAdapter.OnItemClickListener {

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    private var mViewModel: MoviesViewModel? = null
    private var mListAdapter: MoviesListAdapter? = null
    private var mListTypesAdapter: MoviesTypesListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        AndroidInjection.inject(this)

        MoviesTypesListAdapter(this).also {
            mListTypesAdapter = it
            val snapHelper = LinearSnapHelper()
            snapHelper.attachToRecyclerView(rv_type_movies)
            rv_type_movies.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            rv_type_movies.adapter = it
        }
        MoviesListAdapter(this).also {
            mListAdapter = it
            rv_movies.layoutManager = GridLayoutManager(this, 2)
            rv_movies.adapter = it
        }
        ViewModelProviders
            .of(this, mViewModelFactory)
            .get(MoviesViewModel::class.java)
            .also {
                mViewModel = it
                observeViewModel()
            }
    }

    /**
     * Observe the viewModel. GetMovieDetailsList return the movie list in a mutableLiveData.
     * The first time put in the list type movies adapter the first element in the list.
     * When receive the data send the movies to listAdapter
     */
    private fun observeViewModel() {

        mViewModel?.apply {
            getMovieList(getMovieTypeList()[0])
            mListTypesAdapter?.setTitles(getMovieTypeList())
            mListTypesAdapter?.notifyDataSetChanged()

            getMutableMoviesData().observeForever {
                mListAdapter?.setMovies(it)
                mListAdapter?.notifyDataSetChanged()
            }

            getMutableDataError().observeForever {
                Log.e(TAG, it)
            }

            getMutableLoadingMovies().observeForever {
                Log.w(TAG, it.toString())
                progress.visibility = View.GONE
            }
        }
    }

    /**
     * Click on the type movies
     */
    override fun onItemTypeClicked(title: String) {
        mViewModel?.getMovieList(title)
        progress.visibility = View.VISIBLE

    }

    /**
     * Click on the movie.
     */
    override fun onItemClicked(movies: MoviesListBindingModel) {
        mViewModel?.onMovieSelected(movies)
    }

    companion object {
        val TAG = MoviesActivity::class.java.canonicalName
    }
}
