package com.eladiosuarez.rakuten.ui.movies.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesListBindingModel
import com.eladiosuarez.rakuten.data.repository.MovieDataSource
import com.eladiosuarez.rakuten.ui.ActivityNavigator
import com.eladiosuarez.rakuten.ui.movies.mapper.MovieListBindingModelMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

/**
 * MoviesViewModel
 * @author Eladio Freire
 */
class MoviesViewModel(
    application: Application,
    private val mMovieApiDataSource: MovieDataSource,
    private val mActivityNavigator: ActivityNavigator
) : AndroidViewModel(application) {

    private val mDisposableMovies = CompositeDisposable()
    private val mMutableLiveData by lazy { MutableLiveData<List<MoviesListBindingModel>>() }
    private val mMutableLiveDataLoading by lazy { MutableLiveData<Boolean>() }
    private val mMutableLiveDataError by lazy { MutableLiveData<String>() }

    /**
     * Get Movie list. Call to the MovieApiDataSource to get the movie detail.
     * These return an Single observable that put inside disposable and get the events in the subscribeBy and send
     * the events in the MutableLiveData if is error, loading finish or value data.
     * @param id: Type Movie id.
     */
    fun getMovieList(id: String) {

        mDisposableMovies.add(
            mMovieApiDataSource.getMovies(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .flatMapIterable {
                    it.dataMovie.contents.dataContent
                }
                .map {
                    MovieListBindingModelMapper.mapMovieToMovieBindingModel(it)
                }
                .toList()
                .toObservable()
                .subscribeBy(
                    onNext = {
                        mMutableLiveData.value = it
                    },
                    onError = {
                        mMutableLiveDataError.value = it.localizedMessage
                        mMutableLiveDataLoading.setValue(false)
                    },
                    onComplete = {
                        mMutableLiveDataLoading.setValue(false)
                    }
                ))
    }

    /**
     * List Type movies.
     */
    fun getMovieTypeList(): List<String> {
        return arrayListOf(
            "populares-en-taquilla",
            "estrenos-para-toda-la-familia",
            "estrenos-imprescindibles-en-taquilla",
            "estrenos-espanoles",
            "si-te-perdiste",
            "especial-x-men",
            "nuestras-preferidas-de-la-semana"
        )
    }

    /**
     * Get MutableLiveData to return the movie list to the ui.
     */
    fun getMutableMoviesData(): MutableLiveData<List<MoviesListBindingModel>> {
        return mMutableLiveData
    }

    /**
     * Get MutableLiveData to return the errors in the server request to the ui.
     */
    fun getMutableDataError(): MutableLiveData<String> {
        return mMutableLiveDataError
    }

    /**
     * Get MutableLiveData to return when the request has finished.
     */
    fun getMutableLoadingMovies(): MutableLiveData<Boolean> {

        return mMutableLiveDataLoading
    }

    /**
     * Navigate to the detail movie.
     */
    fun onMovieSelected(movies: MoviesListBindingModel) {
        mActivityNavigator.openMovieDetail(movies = movies)
    }

    override fun onCleared() {
        super.onCleared()
        mDisposableMovies.dispose()
    }
}