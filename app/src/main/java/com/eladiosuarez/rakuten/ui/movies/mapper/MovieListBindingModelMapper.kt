package com.eladiosuarez.rakuten.ui.movies.mapper

import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesListBindingModel
import com.eladiosuarez.rakuten.data.model.response.listmovies.DataX

/**
 * MovieListBindingModelMapper
 * @author Eladio Freire
 */
object MovieListBindingModelMapper {


    fun mapMovieToMovieBindingModel(movies: DataX): MoviesListBindingModel {
        return MoviesListBindingModel(
            id = movies.id,
            title = movies.title,
            imageUrl = movies.images.artwork,
            rating = movies.highlightedScore.score.toString(),
            users = movies.highlightedScore.formattedAmountOfVotes

        )
    }
}