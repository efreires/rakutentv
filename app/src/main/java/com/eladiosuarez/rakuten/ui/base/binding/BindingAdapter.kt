package com.eladiosuarez.rakuten.ui.base.binding

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.eladiosuarez.rakuten.R

class BindingAdapter {
    companion object {

        @BindingAdapter("imageUrl")
        @JvmStatic
        fun loadImage(imageView: ImageView, imageUrl: String?) {
            if (imageUrl != null)
                Glide.with(imageView.context)
                    .load(imageUrl)
                    .placeholder(R.drawable.bajo)
                    .into(imageView)
            else
                imageView.setImageResource(R.drawable.ic_local_movies)
        }

        @BindingAdapter("visibilityText")
        @JvmStatic
        fun TextView.setVisibilityText(value: String?) {

            visibility = if (value != null) View.VISIBLE else View.GONE
        }

        @BindingAdapter("capitalizeText")
        @JvmStatic
        fun TextView.setCapitalizeText(value: String?) {

            text = value?.toUpperCase()?.replace("-", " ")
        }
    }
}