package com.eladiosuarez.rakuten.ui.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleRegistry

class BaseFragment : Fragment() {
    var lifecycleRegistry = LifecycleRegistry(this)

}