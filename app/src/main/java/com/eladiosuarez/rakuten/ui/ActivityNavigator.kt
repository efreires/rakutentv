package com.eladiosuarez.rakuten.ui

import android.content.Context
import android.content.Intent
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesListBindingModel
import com.eladiosuarez.rakuten.ui.detailmovie.view.MovieDetailActivity
import com.eladiosuarez.rakuten.ui.video.VideoTrailerFullScreenActivity

interface ActivityNavigator {
    fun openMovieDetail(movies: MoviesListBindingModel)
    fun openMovieTrailer(url: String)
}

class ActivityNavigatorImpl(
    private val mContext: Context
) : ActivityNavigator {
    /**
     * Open Movie Detail.
     */
    override fun openMovieDetail(movies: MoviesListBindingModel) {

        with(Intent(mContext, MovieDetailActivity::class.java)) {
            putExtra(MovieDetailActivity.BUNDLE_MOVIE_ID, movies)
            mContext.startActivity(this)
        }

    }

    /**
     * Open Movie Trailer.
     */
    override fun openMovieTrailer(url: String) {

        with(Intent(mContext, VideoTrailerFullScreenActivity::class.java)) {
            putExtra(VideoTrailerFullScreenActivity.BUNDLE_MOVIE_TRAILER_ID, url)
            mContext.startActivity(this)
        }

    }
}