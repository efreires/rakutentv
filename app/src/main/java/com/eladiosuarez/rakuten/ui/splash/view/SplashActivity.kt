package com.eladiosuarez.rakuten.ui.splash.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.eladiosuarez.rakuten.R
import com.eladiosuarez.rakuten.ui.base.BaseActivity
import com.eladiosuarez.rakuten.ui.movies.view.MoviesActivity

/**
 * SplashActivity
 * @author Eladio Freire
 */
class SplashActivity : BaseActivity() {
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds

    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            val intent = Intent(applicationContext, MoviesActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mDelayHandler = Handler()
        mDelayHandler?.postDelayed(mRunnable, SPLASH_DELAY)

    }

    public override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler?.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }

}