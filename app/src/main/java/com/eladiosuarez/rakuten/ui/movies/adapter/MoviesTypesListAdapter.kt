package com.eladiosuarez.rakuten.ui.movies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.eladiosuarez.rakuten.R
import com.eladiosuarez.rakuten.databinding.ItemMovieTypeBinding

/**
 * MoviesTypesListAdapter
 * @author Eladio Freire
 */
class MoviesTypesListAdapter(
    private val mItemListener: OnItemClickListener?
) : RecyclerView.Adapter<MoviesTypesListAdapter.MoviesTypeVH>() {

    private var mMoviesTypes: List<String> = emptyList()

    interface OnItemClickListener {
        fun onItemTypeClicked(title: String)
    }

    class MoviesTypeVH(
        itemView: View,
        private val binding: ItemMovieTypeBinding,
        private val itemListener: OnItemClickListener?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: String) {
            with(binding) {
                title = item
                itemClickListener = itemListener
                executePendingBindings()
            }
        }
    }

    fun setTitles(movies: List<String>) {
        mMoviesTypes = movies
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesTypeVH {
        return LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_movie_type, parent, false)
            .let { view ->
                val binding = DataBindingUtil.bind<ItemMovieTypeBinding>(view)!!
                MoviesTypeVH(itemView = view, binding = binding, itemListener = mItemListener)
            }
    }

    override fun onBindViewHolder(holder: MoviesTypeVH, position: Int) {
        holder.bind(mMoviesTypes[position])
    }

    override fun getItemCount(): Int = mMoviesTypes.size

    companion object {
        private val TAG = MoviesTypesListAdapter::class.java.canonicalName
    }


}
