package com.eladiosuarez.rakuten.ui.movies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.eladiosuarez.rakuten.R
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesListBindingModel
import com.eladiosuarez.rakuten.databinding.ItemMovieBinding

/**
 * MoviesListAdapter
 * @author Eladio Freire
 */
class MoviesListAdapter(
    private val mItemListener: OnItemClickListener?
) : RecyclerView.Adapter<MoviesListAdapter.MoviesVH>() {

    private var mMovies: List<MoviesListBindingModel> = emptyList()

    interface OnItemClickListener {
        fun onItemClicked(movies: MoviesListBindingModel)
    }

    class MoviesVH(
        itemView: View,
        private val binding: ItemMovieBinding,
        private val itemListener: OnItemClickListener?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: MoviesListBindingModel) {

            with(binding) {
                movies = item
                itemClickListener = itemListener
                executePendingBindings()
            }
        }
    }

    fun setMovies(movies: List<MoviesListBindingModel>) {
        mMovies = movies
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesVH {
        return LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_movie, parent, false)
            .let { view ->
                val binding = DataBindingUtil.bind<ItemMovieBinding>(view)!!
                MoviesVH(
                    itemView = view,
                    binding = binding,
                    itemListener = mItemListener
                )
            }
    }

    override fun onBindViewHolder(holder: MoviesVH, position: Int) {
        holder.bind(mMovies[position])
    }

    override fun getItemCount(): Int = mMovies.size

    companion object {
        private val TAG = MoviesListAdapter::class.java.canonicalName
    }


}
