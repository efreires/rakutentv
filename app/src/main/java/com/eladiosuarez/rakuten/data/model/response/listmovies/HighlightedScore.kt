package com.eladiosuarez.rakuten.data.model.response.listmovies


import com.google.gson.annotations.SerializedName

data class HighlightedScore(
    @SerializedName("amount_of_votes")
    val amountOfVotes: Double,
    @SerializedName("formatted_amount_of_votes")
    val formattedAmountOfVotes: String,
    @SerializedName("score")
    val score: Double
)