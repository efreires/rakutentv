package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Images(
    @SerializedName("artwork")
    val artwork: String,
    @SerializedName("ribbons")
    val ribbons: List<Ribbon>,
    @SerializedName("snapshot")
    val snapshot: String
)