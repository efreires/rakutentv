package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Award(
    @SerializedName("category")
    val category: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("photo")
    val photo: String,
    @SerializedName("result")
    val result: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("verdict")
    val verdict: Verdict,
    @SerializedName("year")
    val year: Int
)