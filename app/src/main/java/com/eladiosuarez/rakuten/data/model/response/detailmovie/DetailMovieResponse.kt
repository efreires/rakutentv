package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class DetailMovieResponse(
    @SerializedName("data")
    val dataDetail: Data,
    @SerializedName("meta")
    val meta: Meta
)