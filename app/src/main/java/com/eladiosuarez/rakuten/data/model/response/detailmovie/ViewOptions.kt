package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class ViewOptions(
    @SerializedName("private")
    val `private`: Private,
    @SerializedName("public")
    val `public`: Public,
    @SerializedName("support")
    val support: Support
)