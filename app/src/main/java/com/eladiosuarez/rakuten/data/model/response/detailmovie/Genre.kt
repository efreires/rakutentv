package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Genre(
    @SerializedName("additional_images")
    val additionalImages: AdditionalImages,
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("erotic")
    val erotic: Boolean,
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("type")
    val type: String
)