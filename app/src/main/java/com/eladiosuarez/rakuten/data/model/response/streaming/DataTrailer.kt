package com.eladiosuarez.rakuten.data.model.response.streaming


import com.google.gson.annotations.SerializedName

data class DataTrailer(
    @SerializedName("heartbeat")
    val heartbeat: Heartbeat,
    @SerializedName("id")
    val id: String,
    @SerializedName("stream_infos")
    val streamInfos: List<StreamInfo>,
    @SerializedName("type")
    val type: String
)