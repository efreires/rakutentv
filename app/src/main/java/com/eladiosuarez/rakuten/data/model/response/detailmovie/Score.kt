package com.eladiosuarez.rakuten.data.model.response.detailmovie


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Score(
    @SerializedName("amount_of_votes")
    val amountOfVotes: Int,
    @SerializedName("formatted_amount_of_votes")
    val formattedAmountOfVotes: String = "",
    @SerializedName("highlighted")
    val highlighted: Boolean,
    @SerializedName("href")
    val href: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("score")
    val score: Double,
    @SerializedName("site")
    val site: Site,
    @SerializedName("type")
    val type: String
) : Parcelable