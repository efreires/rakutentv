package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Public(
    @SerializedName("adverts")
    val adverts: List<Any>,
    @SerializedName("previews")
    val previews: List<Any>,
    @SerializedName("trailers")
    val trailers: List<Trailer>
)