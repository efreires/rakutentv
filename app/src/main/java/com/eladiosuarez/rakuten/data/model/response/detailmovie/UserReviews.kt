package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class UserReviews(
    @SerializedName("data")
    val `data`: List<DataX>,
    @SerializedName("meta")
    val meta: MetaX
)