package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class DataX(
    @SerializedName("body")
    val body: String,
    @SerializedName("href")
    val href: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("person")
    val person: Person,
    @SerializedName("score")
    val score: Int,
    @SerializedName("source")
    val source: Source,
    @SerializedName("type")
    val type: String
)