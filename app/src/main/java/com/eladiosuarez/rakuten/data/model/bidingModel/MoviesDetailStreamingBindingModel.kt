package com.eladiosuarez.rakuten.data.model.bidingModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MoviesDetailStreamingBindingModel(
    val audioLanguage: String,
    val audioQuality: String,
    val contentId: String,
    val contentType: String,
    val deviceSerial: String,
    val deviceStreamVideoQuality: String,
    val player: String,
    val subtitleLanguage: String,
    val videoQuality: String,
    val videoType: String
) : Parcelable
