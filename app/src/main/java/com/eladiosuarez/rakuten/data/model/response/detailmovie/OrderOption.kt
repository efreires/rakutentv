package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class OrderOption(
    @SerializedName("external_tiers")
    val externalTiers: List<ExternalTier>,
    @SerializedName("id")
    val id: String,
    @SerializedName("legacy_instance_id")
    val legacyInstanceId: Any,
    @SerializedName("legacy_instance_type")
    val legacyInstanceType: Any,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("order_option_display_info")
    val orderOptionDisplayInfo: OrderOptionDisplayInfo,
    @SerializedName("periodic_points_reward_amount")
    val periodicPointsRewardAmount: Int,
    @SerializedName("points")
    val points: Points,
    @SerializedName("price")
    val price: String,
    @SerializedName("price_without_currency")
    val priceWithoutCurrency: Double,
    @SerializedName("purchase_type")
    val purchaseType: PurchaseType,
    @SerializedName("type")
    val type: String,
    @SerializedName("video_quality")
    val videoQuality: VideoQuality,
    @SerializedName("warnings")
    val warnings: List<Warning>
)