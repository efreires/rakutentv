package com.eladiosuarez.rakuten.data.model.response.detailmovie


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Site(
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("scale")
    val scale: Int,
    @SerializedName("show_image")
    val showImage: Boolean,
    @SerializedName("type")
    val type: String
) : Parcelable