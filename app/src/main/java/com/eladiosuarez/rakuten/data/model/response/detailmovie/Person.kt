package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Person(
    @SerializedName("born_at")
    val bornAt: Any,
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("photo")
    val photo: String,
    @SerializedName("type")
    val type: String
)