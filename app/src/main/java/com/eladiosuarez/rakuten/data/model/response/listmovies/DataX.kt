package com.eladiosuarez.rakuten.data.model.response.listmovies


import com.google.gson.annotations.SerializedName

data class DataX(
    @SerializedName("classification")
    val classification: Classification,
    @SerializedName("duration")
    val duration: Int,
    @SerializedName("highlighted_score")
    val highlightedScore: HighlightedScore,
    @SerializedName("id")
    val id: String,
    @SerializedName("images")
    val images: Images,
    @SerializedName("label")
    val label: String,
    @SerializedName("labels")
    val labels: Labels,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("rating")
    val rating: Rating,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("year")
    val year: Int
)