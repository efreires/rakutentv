package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class SubtitleLanguage(
    @SerializedName("abbr")
    val abbr: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("type")
    val type: String
)