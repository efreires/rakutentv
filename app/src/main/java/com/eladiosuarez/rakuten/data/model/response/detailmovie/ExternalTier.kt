package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class ExternalTier(
    @SerializedName("id")
    val id: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("type")
    val type: String
)