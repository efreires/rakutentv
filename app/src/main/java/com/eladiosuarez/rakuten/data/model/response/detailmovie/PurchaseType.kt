package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class PurchaseType(
    @SerializedName("available_time_in_seconds")
    val availableTimeInSeconds: Any,
    @SerializedName("expires_after_in_seconds")
    val expiresAfterInSeconds: Any,
    @SerializedName("id")
    val id: String,
    @SerializedName("is_recurring")
    val isRecurring: Boolean,
    @SerializedName("kind")
    val kind: String,
    @SerializedName("label")
    val label: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("type")
    val type: String
)