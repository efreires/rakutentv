package com.eladiosuarez.rakuten.data.repository

import com.eladiosuarez.rakuten.data.MoviesApi
import com.eladiosuarez.rakuten.data.model.request.streaming.StreamingRequest
import com.eladiosuarez.rakuten.data.model.response.detailmovie.DetailMovieResponse
import com.eladiosuarez.rakuten.data.model.response.listmovies.ListMovies
import com.eladiosuarez.rakuten.data.model.response.streaming.StreamingResponse
import com.eladiosuarez.rakuten.utils.Constants.Companion.CLASIFICATION_ID
import com.eladiosuarez.rakuten.utils.Constants.Companion.DEVICE_IDENTIFIER
import com.eladiosuarez.rakuten.utils.Constants.Companion.LANGUAGE
import io.reactivex.Single


class MovieRepository(var api: MoviesApi) : MovieDataSource {

    override fun getMovies(id: String): Single<ListMovies> {
        return api.getMoviesList(id, CLASIFICATION_ID, DEVICE_IDENTIFIER, LANGUAGE, LANGUAGE)

    }

    override fun getMovieDetail(idMovie: String): Single<DetailMovieResponse> {
        return api.getMovieDetailList(idMovie, CLASIFICATION_ID, DEVICE_IDENTIFIER, LANGUAGE, LANGUAGE)
    }

    override fun postTrailerStreaming(streamingRequest: StreamingRequest): Single<StreamingResponse> {
        return api.postTrailerStreaming(streamingRequest, CLASIFICATION_ID, DEVICE_IDENTIFIER, LANGUAGE, LANGUAGE)
    }

}
