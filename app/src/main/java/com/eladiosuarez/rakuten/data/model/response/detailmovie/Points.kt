package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Points(
    @SerializedName("cost")
    val cost: Int,
    @SerializedName("reward")
    val reward: Int
)