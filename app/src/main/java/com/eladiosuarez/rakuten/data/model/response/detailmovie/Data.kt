package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("actors")
    val actors: List<Actor>,
    @SerializedName("additional_images")
    val additionalImages: AdditionalImages,
    @SerializedName("awards")
    val awards: List<Award>,
    @SerializedName("classification")
    val classification: Classification,
    @SerializedName("countries")
    val countries: List<Country>,
    @SerializedName("critic_reviews")
    val criticReviews: CriticReviews,
    @SerializedName("deep_links")
    val deepLinks: DeepLinks,
    @SerializedName("directors")
    val directors: List<Director>,
    @SerializedName("duration")
    val duration: Int,
    @SerializedName("duration_in_seconds")
    val durationInSeconds: Any,
    @SerializedName("extras")
    val extras: List<Any>,
    @SerializedName("genres")
    val genres: List<Genre>,
    @SerializedName("groups")
    val groups: List<Any>,
    @SerializedName("highlight")
    val highlight: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("images")
    val images: Images,
    @SerializedName("labels")
    val labels: Labels,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("offline_enabled_for_est")
    val offlineEnabledForEst: Boolean,
    @SerializedName("offline_enabled_for_rent")
    val offlineEnabledForRent: Boolean,
    @SerializedName("offline_enabled_for_svod")
    val offlineEnabledForSvod: Boolean,
    @SerializedName("order_options")
    val orderOptions: List<OrderOption>,
    @SerializedName("original_title")
    val originalTitle: String,
    @SerializedName("plot")
    val plot: String,
    @SerializedName("rating")
    val rating: Rating,
    @SerializedName("scores")
    val scores: List<Score>,
    @SerializedName("short_plot")
    val shortPlot: String,
    @SerializedName("subscription_plans")
    val subscriptionPlans: List<Any>,
    @SerializedName("svod_schedules")
    val svodSchedules: List<Any>,
    @SerializedName("tags")
    val tags: List<Any>,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("ultraviolet_enabled")
    val ultravioletEnabled: Boolean,
    @SerializedName("user_reviews")
    val userReviews: UserReviews,
    @SerializedName("view_options")
    val viewOptions: ViewOptions,
    @SerializedName("year")
    val year: Int
)