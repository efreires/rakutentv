package com.eladiosuarez.rakuten.data.model.request.streaming


import com.google.gson.annotations.SerializedName

data class StreamingRequest(
    @SerializedName("audio_language")
    val audioLanguage: String,
    @SerializedName("audio_quality")
    val audioQuality: String,
    @SerializedName("content_id")
    val contentId: String,
    @SerializedName("content_type")
    val contentType: String,
    @SerializedName("device_serial")
    val deviceSerial: String,
    @SerializedName("device_stream_video_quality")
    val deviceStreamVideoQuality: String,
    @SerializedName("player")
    val player: String,
    @SerializedName("subtitle_language")
    val subtitleLanguage: String,
    @SerializedName("video_type")
    val videoType: String
)