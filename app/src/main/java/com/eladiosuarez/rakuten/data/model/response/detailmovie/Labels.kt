package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Labels(
    @SerializedName("audio_qualities")
    val audioQualities: List<AudioQuality>,
    @SerializedName("hdr_types")
    val hdrTypes: List<HdrType>,
    @SerializedName("purchase_types")
    val purchaseTypes: List<PurchaseType>,
    @SerializedName("video_qualities")
    val videoQualities: List<VideoQuality>
)