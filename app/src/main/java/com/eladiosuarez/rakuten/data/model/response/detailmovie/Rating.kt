package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Rating(
    @SerializedName("average")
    val average: Int,
    @SerializedName("scale")
    val scale: Int
)