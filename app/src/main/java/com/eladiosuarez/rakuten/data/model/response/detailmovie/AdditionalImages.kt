package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class AdditionalImages(
    @SerializedName("icon")
    val icon: String
)