package com.eladiosuarez.rakuten.data.model.response.listmovies


import com.google.gson.annotations.SerializedName

data class Contents(
    @SerializedName("data")
    val dataContent: List<DataX>,
    @SerializedName("meta")
    val meta: Meta
)