package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Support(
    @SerializedName("audio_qualities")
    val audioQualities: List<AudioQuality>,
    @SerializedName("hdr_types")
    val hdrTypes: List<HdrType>,
    @SerializedName("video_qualities")
    val videoQualities: List<VideoQuality>
)