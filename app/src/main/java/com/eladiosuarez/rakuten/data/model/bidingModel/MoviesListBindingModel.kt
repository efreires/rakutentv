package com.eladiosuarez.rakuten.data.model.bidingModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MoviesListBindingModel(
    val id: String,
    val title: String,
    val imageUrl: String? = null,
    val rating: String? = null,
    val users: String? = null
) : Parcelable
