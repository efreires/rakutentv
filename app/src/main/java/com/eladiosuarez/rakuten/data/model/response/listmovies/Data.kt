package com.eladiosuarez.rakuten.data.model.response.listmovies


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("additional_images")
    val additionalImages: AdditionalImages,
    @SerializedName("category")
    val category: Any,
    @SerializedName("content_type")
    val contentType: String,
    @SerializedName("contents")
    val contents: Contents,
    @SerializedName("id")
    val id: String,
    @SerializedName("is_b2b")
    val isB2b: Boolean,
    @SerializedName("is_recommendation")
    val isRecommendation: Boolean,
    @SerializedName("kind")
    val kind: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("only_coupon")
    val onlyCoupon: Boolean,
    @SerializedName("short_name")
    val shortName: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("wktv_code")
    val wktvCode: Any
)