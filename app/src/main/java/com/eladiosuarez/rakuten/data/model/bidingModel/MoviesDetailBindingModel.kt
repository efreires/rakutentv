package com.eladiosuarez.rakuten.data.model.bidingModel

import android.os.Parcelable
import com.eladiosuarez.rakuten.data.model.response.detailmovie.Score
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MoviesDetailBindingModel(
    val streamingPost: MoviesDetailStreamingBindingModel,
    val score: List<Score>,
    val genre: List<String>,
    val synopsis: String,
    val actors: List<String>,
    val title: String,
    val originalTitle: String,
    val language: List<String>,
    val country: String,
    val duration: String,
    val rent: String,
    val sell: String,
    val age: String,
    val year: String,
    val poster: String,
    val snapshot: String

) : Parcelable
