package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Warning(
    @SerializedName("field")
    val `field`: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String
)