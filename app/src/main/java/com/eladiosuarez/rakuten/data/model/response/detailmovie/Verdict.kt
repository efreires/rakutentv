package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Verdict(
    @SerializedName("localized_name")
    val localizedName: String,
    @SerializedName("name")
    val name: String
)