package com.eladiosuarez.rakuten.data.model.response.listmovies


import com.google.gson.annotations.SerializedName

data class Rating(
    @SerializedName("average")
    val average: Int,
    @SerializedName("scale")
    val scale: Int
)