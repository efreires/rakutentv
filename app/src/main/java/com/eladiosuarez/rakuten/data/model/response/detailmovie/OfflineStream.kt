package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class OfflineStream(
    @SerializedName("audio_languages")
    val audioLanguages: List<AudioLanguage>,
    @SerializedName("audio_qualities")
    val audioQualities: List<AudioQuality>,
    @SerializedName("hdr_types")
    val hdrTypes: List<HdrType>,
    @SerializedName("streaming_drm_types")
    val streamingDrmTypes: List<StreamingDrmType>,
    @SerializedName("subtitle_languages")
    val subtitleLanguages: List<SubtitleLanguage>,
    @SerializedName("video_qualities")
    val videoQualities: List<VideoQuality>
)