package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class StreamingDrmType(
    @SerializedName("id")
    val id: String,
    @SerializedName("type")
    val type: String
)