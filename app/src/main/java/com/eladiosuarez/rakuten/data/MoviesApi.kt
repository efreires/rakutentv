package com.eladiosuarez.rakuten.data


import com.eladiosuarez.rakuten.data.model.request.streaming.StreamingRequest
import com.eladiosuarez.rakuten.data.model.response.detailmovie.DetailMovieResponse
import com.eladiosuarez.rakuten.data.model.response.listmovies.ListMovies
import com.eladiosuarez.rakuten.data.model.response.streaming.StreamingResponse
import io.reactivex.Single
import retrofit2.http.*


interface MoviesApi {

    @GET("v3/lists/{id}")
    fun getMoviesList(
        @Path("id") id: String,
        @Query("classification_id") classificationId: Int,
        @Query("device_identifier") device_identifier: String,
        @Query("locale") locale: String,
        @Query("market_code") marketCode: String
    ): Single<ListMovies>

    @GET("v3/movies/{id}")
    fun getMovieDetailList(
        @Path("id") id: String,
        @Query("classification_id") classificationId: Int,
        @Query("device_identifier") device_identifier: String,
        @Query("locale") locale: String,
        @Query("market_code") marketCode: String
    ): Single<DetailMovieResponse>

    @POST("v3/me/streamings")
    fun postTrailerStreaming(
        @Body streamingRequest: StreamingRequest,
        @Query("classification_id") classificationId: Int,
        @Query("device_identifier") device_identifier: String,
        @Query("locale") locale: String,
        @Query("market_code") marketCode: String
    ): Single<StreamingResponse>
}