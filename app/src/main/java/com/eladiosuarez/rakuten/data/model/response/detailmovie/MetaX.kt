package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class MetaX(
    @SerializedName("already_seens")
    val alreadySeens: List<Any>,
    @SerializedName("heartbeats")
    val heartbeats: List<Any>,
    @SerializedName("likes")
    val likes: List<Any>,
    @SerializedName("rights")
    val rights: List<Any>,
    @SerializedName("warnings")
    val warnings: List<Any>,
    @SerializedName("wishlists")
    val wishlists: List<Any>
)