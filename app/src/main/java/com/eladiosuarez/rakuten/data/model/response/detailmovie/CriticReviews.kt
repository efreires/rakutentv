package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class CriticReviews(
    @SerializedName("data")
    val dataCritic: List<Any>,
    @SerializedName("meta")
    val meta: Meta
)