package com.eladiosuarez.rakuten.data.repository

import com.eladiosuarez.rakuten.data.model.request.streaming.StreamingRequest
import com.eladiosuarez.rakuten.data.model.response.detailmovie.DetailMovieResponse
import com.eladiosuarez.rakuten.data.model.response.listmovies.ListMovies
import com.eladiosuarez.rakuten.data.model.response.streaming.StreamingResponse
import io.reactivex.Single

interface MovieDataSource {

    fun getMovies(id: String): Single<ListMovies>
    fun getMovieDetail(idMovie: String): Single<DetailMovieResponse>
    fun postTrailerStreaming(streamingRequest: StreamingRequest): Single<StreamingResponse>

}