package com.eladiosuarez.rakuten.data.model.response.streaming


import com.google.gson.annotations.SerializedName

data class StreamingResponse(
    @SerializedName("data")
    val mData: DataTrailer
)