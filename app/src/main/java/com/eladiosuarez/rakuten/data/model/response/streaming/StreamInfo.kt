package com.eladiosuarez.rakuten.data.model.response.streaming


import com.google.gson.annotations.SerializedName

data class StreamInfo(
    @SerializedName("all_subtitles")
    val allSubtitles: List<Any>,
    @SerializedName("cdn")
    val cdn: String,
    @SerializedName("display_aspect_ratio")
    val displayAspectRatio: String,
    @SerializedName("player")
    val player: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("video_quality")
    val videoQuality: String,
    @SerializedName("wrid")
    val wrid: String
)