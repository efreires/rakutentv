package com.eladiosuarez.rakuten.data.model.response.listmovies


import com.google.gson.annotations.SerializedName

data class ListMovies(
    @SerializedName("data")
    val dataMovie: Data
)