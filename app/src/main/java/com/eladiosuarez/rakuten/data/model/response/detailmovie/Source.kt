package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Source(
    @SerializedName("id")
    val id: String,
    @SerializedName("logo")
    val logo: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numerical_id")
    val numericalId: Int,
    @SerializedName("score_scale")
    val scoreScale: Int,
    @SerializedName("type")
    val type: String,
    @SerializedName("url")
    val url: String
)