package com.eladiosuarez.rakuten.data.model.response.detailmovie


import com.google.gson.annotations.SerializedName

data class Private(
    @SerializedName("offline_streams")
    val offlineStreams: List<OfflineStream>,
    @SerializedName("streams")
    val streams: List<Stream>
)