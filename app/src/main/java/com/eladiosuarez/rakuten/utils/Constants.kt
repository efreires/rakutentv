package com.eladiosuarez.rakuten.utils

class Constants {
    //6, "android", "es", "es"

    companion object {
        const val CLASIFICATION_ID = 6
        const val DEVICE_IDENTIFIER = "android"
        const val LANGUAGE = "es"
    }
}