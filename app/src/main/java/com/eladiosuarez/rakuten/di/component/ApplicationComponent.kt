package com.eladiosuarez.rakuten.di.component

import com.eladiosuarez.rakuten.MoviesApp
import com.eladiosuarez.rakuten.di.module.ActivityBuilder
import com.eladiosuarez.rakuten.di.module.AndroidModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidModule::class,
        ActivityBuilder::class,
        AndroidInjectionModule::class]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: MoviesApp): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: MoviesApp)

}