package com.eladiosuarez.rakuten.di.module

import androidx.lifecycle.ViewModelProvider
import com.eladiosuarez.rakuten.MoviesApp
import com.eladiosuarez.rakuten.data.repository.MovieDataSource
import com.eladiosuarez.rakuten.ui.ActivityNavigator
import dagger.Module
import dagger.Provides
import com.eladiosuarez.rakuten.data.MoviesApi
import com.eladiosuarez.rakuten.data.repository.MovieRepository
import com.eladiosuarez.rakuten.viewmodel.MovieViewModelFactory


@Module
class RepositoryModule {

    @Provides
    fun provideMovieApiService(api: MoviesApi): MovieDataSource {

        return MovieRepository(api = api)
    }

    @Provides
    fun provideViewModelFactory(
        movielDataSource: MovieDataSource,
        application: MoviesApp,
        activityNavigator: ActivityNavigator
    ): ViewModelProvider.Factory {
        return MovieViewModelFactory(
            movielDataSource = movielDataSource,
            application = application,
            activityNavigator = activityNavigator
        )
    }

}
