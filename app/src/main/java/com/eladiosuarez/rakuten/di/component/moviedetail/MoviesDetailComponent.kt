package com.eladiosuarez.rakuten.di.component.moviedetail

import com.eladiosuarez.rakuten.di.module.ApiModule
import com.eladiosuarez.rakuten.di.module.NetworkModule
import com.eladiosuarez.rakuten.di.module.RepositoryModule
import com.eladiosuarez.rakuten.di.scopes.ListScope
import com.eladiosuarez.rakuten.ui.detailmovie.view.MovieDetailActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent(
    modules = [
        NetworkModule::class,
        ApiModule::class,
        RepositoryModule::class]
)
@ListScope
interface MoviesDetailComponent : AndroidInjector<MovieDetailActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MovieDetailActivity>()
}