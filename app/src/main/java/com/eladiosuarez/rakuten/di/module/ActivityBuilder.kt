package com.eladiosuarez.rakuten.di.module

import android.app.Activity
import com.eladiosuarez.rakuten.di.component.moviedetail.MoviesDetailComponent
import com.eladiosuarez.rakuten.di.component.movies.MoviesComponent
import com.eladiosuarez.rakuten.ui.detailmovie.view.MovieDetailActivity
import com.eladiosuarez.rakuten.ui.movies.view.MoviesActivity
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class ActivityBuilder {

    @Binds
    @IntoMap
    @ActivityKey(MoviesActivity::class)
    abstract fun bindListActivity(
        builder: MoviesComponent.Builder
    ): AndroidInjector.Factory<out Activity>

    @Binds
    @IntoMap
    @ActivityKey(MovieDetailActivity::class)
    abstract fun bindDetailActivity(
        builder: MoviesDetailComponent.Builder
    ): AndroidInjector.Factory<out Activity>

}