package com.eladiosuarez.rakuten.di.module

import android.content.Context
import android.content.res.Resources
import com.eladiosuarez.rakuten.MoviesApp
import com.eladiosuarez.rakuten.di.component.moviedetail.MoviesDetailComponent
import com.eladiosuarez.rakuten.di.component.movies.MoviesComponent
import com.eladiosuarez.rakuten.ui.ActivityNavigator
import com.eladiosuarez.rakuten.ui.ActivityNavigatorImpl
import dagger.Module
import dagger.Provides


@Module(
    subcomponents = [
        MoviesComponent::class,
        MoviesDetailComponent::class
    ]
)
class AndroidModule {

    @Provides
    fun provideContext(application: MoviesApp): Context = application

    @Provides
    fun provideResources(application: MoviesApp): Resources = application.resources

    @Provides
    fun provideNavigator(application: MoviesApp): ActivityNavigator = ActivityNavigatorImpl(mContext = application)

}
