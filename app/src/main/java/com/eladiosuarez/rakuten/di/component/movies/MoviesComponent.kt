package com.eladiosuarez.rakuten.di.component.movies

import com.eladiosuarez.rakuten.di.module.ApiModule
import com.eladiosuarez.rakuten.di.module.NetworkModule
import com.eladiosuarez.rakuten.di.module.RepositoryModule
import com.eladiosuarez.rakuten.di.scopes.ListScope
import com.eladiosuarez.rakuten.ui.movies.view.MoviesActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent(
    modules = [
        NetworkModule::class,
        ApiModule::class,
        RepositoryModule::class]
)
@ListScope
interface MoviesComponent : AndroidInjector<MoviesActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MoviesActivity>()
}