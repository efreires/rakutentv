package com.eladiosuarez.rakuten.movies.view

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import com.eladiosuarez.rakuten.R
import com.eladiosuarez.rakuten.ui.movies.view.MoviesActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class MoviesActivityTest {

    @get:Rule
    val activityTestRule = object : ActivityTestRule<MoviesActivity>(
        MoviesActivity::class.java,
        true,
        true
    ) {}


    @Test
    fun scrollToPosition() {
        Espresso.onView(
            ViewMatchers.withId(R.id.rv_movies)
        ).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10)
        )
    }
}