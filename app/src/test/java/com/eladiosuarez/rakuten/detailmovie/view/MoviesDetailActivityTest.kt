package com.eladiosuarez.rakuten.detailmovie.view

import android.app.Activity
import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.eladiosuarez.rakuten.CollapsingToolbarViewMatcher
import com.eladiosuarez.rakuten.data.MoviesApi
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesDetailBindingModel
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesListBindingModel
import com.eladiosuarez.rakuten.data.model.response.detailmovie.DetailMovieResponse
import com.eladiosuarez.rakuten.data.model.response.detailmovie.Score
import com.eladiosuarez.rakuten.data.model.response.detailmovie.Site
import com.eladiosuarez.rakuten.ui.detailmovie.mapper.MovieDetailBindingModelMapper
import com.eladiosuarez.rakuten.ui.detailmovie.view.MovieDetailActivity
import com.google.android.material.appbar.CollapsingToolbarLayout
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.DispatchingAndroidInjector_Factory
import io.reactivex.Single
import org.hamcrest.Matchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import javax.inject.Provider

@RunWith(RobolectricTestRunner::class)

class MoviesDetailActivityTest {

    //var response: String =
    //    "\"meta\": { \"rights\": [], \"heartbeats\": [], \"already_seens\": [], \"wishlists\": [], \"likes\": [], \"warnings\": [] }, \"data\": { \"type\": \"movies\", \"id\": \"el-protegido\", \"numerical_id\": 4337, \"title\": \"El protegido\", \"original_title\": \"Unbreakable\", \"year\": 2000, \"short_plot\": \"David Dunn (Bruce Willis) se convierte en el \u00FAnico superviviente de un terrible accidente de tren. Es cuando un misterioso desconocido, Elijah Price (Samuel L. Jckson) irrumpe en su vida, cont\u00E1ndole una extra\u00F1a teor\u00EDa del por qu\u00E9 no ha sufrido ni un rasgu\u00F1o...\", \"plot\": \"Bruce Willis y Samuel L. Jackson protagonizan esta sorprendente pel\u00EDcula de suspense. David Dunn (Willis) se convierte en el \u00FAnico superviviente de un terrible accidente de tren. Es cuando un misterioso desconocido, Elijah Price (Jackson) irrumpe en su vida, cont\u00E1ndole una extra\u00F1a teor\u00EDa que justifica el que David no haya sufrido ni un solo rasgu\u00F1o en el brutal accidente. La inquietante presencia de Elijah forzar\u00E1 a David a enfrentarse a su destino, en un viaje que te sorprender\u00E1 por su final.\", \"duration\": 102, \"highlight\": null, \"duration_in_seconds\": 6135, \"ultraviolet_enabled\": false, \"groups\": [], \"actors\": [ { \"type\": \"people\", \"id\": \"charlayne-woodard\", \"numerical_id\": 10533, \"photo\": \"https://images-0.wuaki.tv/system/photos/10533/original/charlayne-woodard-1525287242.jpeg\", \"name\": \"Charlayne Woodard\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"eamonn-walker\", \"numerical_id\": 14136, \"photo\": \"https://images-3.wuaki.tv/system/photos/14136/original/eamonn-walker-1525291231.jpeg\", \"name\": \"Eamonn Walker\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"johnny-hiram-jamison\", \"numerical_id\": 128810, \"photo\": \"https://images-0.wuaki.tv/system/photos/128810/original/johnny-hiram-jamison-1548994654.jpeg\", \"name\": \"Johnny Hiram Jamison\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"michaelia-carroll\", \"numerical_id\": 128811, \"photo\": \"https://images-3.wuaki.tv/system/photos/128811/original/michaelia-carroll-1543637592.jpeg\", \"name\": \"Michaelia Carroll\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"bostin-christopher\", \"numerical_id\": 128812, \"photo\": \"https://images-2.wuaki.tv/system/photos/128812/original/bostin-christopher-1525308002.jpeg\", \"name\": \"Bostin Christopher\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"elizabeth-lawrence\", \"numerical_id\": 10297, \"photo\": \"https://images-3.wuaki.tv/system/photos/10297/original/elizabeth-lawrence-1465230729.jpeg\", \"name\": \"Elizabeth Lawrence\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"davis-duffield\", \"numerical_id\": 128813, \"photo\": \"https://images-2.wuaki.tv/system/photos/128813/original/davis-duffield-1543637593.jpeg\", \"name\": \"Davis Duffield\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"laura-regan\", \"numerical_id\": 128814, \"photo\": \"https://images-3.wuaki.tv/system/photos/128814/original/laura-regan-1525308004.jpeg\", \"name\": \"Laura Regan\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"chance-kelly\", \"numerical_id\": 128815, \"photo\": \"https://images-2.wuaki.tv/system/photos/128815/original/chance-kelly-1525308006.jpeg\", \"name\": \"Chance Kelly\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"michael-kelly\", \"numerical_id\": 7965, \"photo\": \"https://images-2.wuaki.tv/system/photos/7965/original/michael-kelly-1525284060.jpeg\", \"name\": \"Michael Kelly\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"bruce-willis\", \"numerical_id\": 700, \"photo\": \"https://images-3.wuaki.tv/system/photos/700/original/bruce-willis-1525275614.jpeg\", \"name\": \"Bruce Willis\", \"born_at\": \"1955-03-19\" }, { \"type\": \"people\", \"id\": \"samuel-l-jackson\", \"numerical_id\": 392, \"photo\": \"https://images-0.wuaki.tv/system/photos/392/original/samuel-l-jackson-1525275246.jpeg\", \"name\": \"Samuel L. Jackson\", \"born_at\": \"1948-12-21\" }, { \"type\": \"people\", \"id\": \"robin-wright\", \"numerical_id\": 1392, \"photo\": \"https://images-1.wuaki.tv/system/photos/1392/original/robin-wright-1525276454.jpeg\", \"name\": \"Robin Wright\", \"born_at\": null }, { \"type\": \"people\", \"id\": \"spencer-treat-clark\", \"numerical_id\": 2353, \"photo\": \"https://images-1.wuaki.tv/system/photos/2353/original/spencer-treat-clark-1525277589.jpeg\", \"name\": \"Spencer Treat Clark\", \"born_at\": null } ], \"awards\": [], \"classification\": { \"type\": \"classifications\", \"id\": \"4\", \"numerical_id\": 4, \"name\": \"13\", \"age\": 13, \"adult\": false, \"description\": \"Mostrar s\u00F3lo los contenidos autorizados para los ni\u00F1os de hasta 13 a\u00F1os\", \"default\": false }, \"countries\": [ { \"type\": \"countries\", \"id\": \"united-states\", \"numerical_id\": 3, \"name\": \"Estados Unidos\" } ], \"deep_links\": {}, \"extras\": [], \"directors\": [ { \"type\": \"people\", \"id\": \"m-night-shyamalan\", \"numerical_id\": 1163, \"photo\": \"https://images-2.wuaki.tv/system/photos/1163/original/m-night-shyamalan-1525276168.jpeg\", \"name\": \"M. Night Shyamalan\", \"born_at\": null } ], \"genres\": [ { \"type\": \"genres\", \"id\": \"autor-indie\", \"numerical_id\": 55, \"name\": \"Autor / Indie\", \"adult\": false, \"erotic\": false, \"additional_images\": { \"icon\": \"https://images-0.wuaki.tv/system/brandable_photos/6351/original/1461242480-1461242480.png\" } }, { \"type\": \"genres\", \"id\": \"drama\", \"numerical_id\": 28, \"name\": \"Drama\", \"adult\": false, \"erotic\": false, \"additional_images\": { \"icon\": \"https://images-2.wuaki.tv/system/brandable_photos/6364/original/1461244827-1461244827.png\" } }, { \"type\": \"genres\", \"id\": \"thriller\", \"numerical_id\": 44, \"name\": \"Thriller\", \"adult\": false, \"erotic\": false, \"additional_images\": { \"icon\": \"https://images-0.wuaki.tv/system/brandable_photos/6372/original/1461246595-1461246595.png\" } } ], \"images\": { \"artwork\": \"https://images-2.wuaki.tv/system/artworks/4337/master/el-protegido-1558429546.jpeg\", \"snapshot\": \"https://images-3.wuaki.tv/system/shots/127408/original/el-protegido-1551183774.jpeg\", \"ribbons\": [] }, \"offline_enabled_for_est\": true, \"offline_enabled_for_rent\": true, \"offline_enabled_for_svod\": false, \"order_options\": [ { \"type\": \"order_options\", \"id\": \"12907\", \"numerical_id\": 12907, \"legacy_instance_type\": null, \"legacy_instance_id\": null, \"periodic_points_reward_amount\": 0, \"price\": \"1,99 \u20AC\", \"price_without_currency\": 1.99, \"points\": { \"cost\": 199, \"reward\": 1 }, \"purchase_type\": { \"type\": \"purchase_types\", \"id\": \"1\", \"numerical_id\": 1, \"is_recurring\": false, \"name\": \"Alquiler 48H\", \"label\": \"ALQUILER 48H\", \"kind\": \"rental\", \"expires_after_in_seconds\": 172800, \"available_time_in_seconds\": 172800 }, \"video_quality\": { \"type\": \"video_qualities\", \"id\": \"SD\", \"numerical_id\": 2, \"name\": \"SD\", \"abbr\": \"SD\", \"position\": 0, \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/sd-1456155688.png\" }, \"external_tiers\": [ { \"type\": \"microsoft_store\", \"id\": \"9NL22WBFK69P\", \"price\": \"2,29 \u20AC\" }, { \"type\": \"itunes\", \"id\": \"tv.wuaki.alternatetier1\", \"price\": \"1,99 \u20AC\" } ], \"warnings\": [], \"order_option_display_info\": {} }, { \"type\": \"order_options\", \"id\": \"572963\", \"numerical_id\": 572963, \"legacy_instance_type\": null, \"legacy_instance_id\": null, \"periodic_points_reward_amount\": 0, \"price\": \"8,99 \u20AC\", \"price_without_currency\": 8.99, \"points\": { \"cost\": 899, \"reward\": 8 }, \"purchase_type\": { \"type\": \"purchase_types\", \"id\": \"2\", \"numerical_id\": 2, \"is_recurring\": false, \"name\": \"Venta (Digital Locker)\", \"label\": \"VENTA (DIGITAL LOCKER)\", \"kind\": \"purchase\", \"expires_after_in_seconds\": null, \"available_time_in_seconds\": null }, \"video_quality\": { \"type\": \"video_qualities\", \"id\": \"SD\", \"numerical_id\": 2, \"name\": \"SD\", \"abbr\": \"SD\", \"position\": 0, \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/sd-1456155688.png\" }, \"external_tiers\": [ { \"type\": \"microsoft_store\", \"id\": \"9NBML3DWKNH5\", \"price\": \"8,99 \u20AC\" }, { \"type\": \"itunes\", \"id\": \"tv.wuaki.tier8\", \"price\": \"8,99 \u20AC\" } ], \"warnings\": [], \"order_option_display_info\": {} } ], \"rating\": { \"average\": 4, \"scale\": 5 }, \"critic_reviews\": { \"meta\": { \"pagination\": { \"page\": 1, \"count\": 0, \"per_page\": 28, \"offset\": 0, \"total_pages\": 0 } }, \"data\": [] }, \"user_reviews\": { \"meta\": { \"pagination\": { \"page\": 1, \"count\": 0, \"per_page\": 28, \"offset\": 0, \"total_pages\": 0 } }, \"data\": [] }, \"scores\": [ { \"type\": \"scores\", \"id\": \"6111\", \"numerical_id\": 6111, \"href\": \"http://www.imdb.com/title/tt0217869/?ref_=fn_al_tt_1\", \"amount_of_votes\": null, \"formatted_amount_of_votes\": null, \"score\": 7.2, \"highlighted\": false, \"site\": { \"type\": \"sites\", \"id\": \"1\", \"numerical_id\": 1, \"name\": \"IMDb\", \"show_image\": false, \"scale\": 10, \"image\": \"https://images-0.wuaki.tv/system/images/1/original/imdb-1481731825.png\" } }, { \"type\": \"scores\", \"id\": \"6112\", \"numerical_id\": 6112, \"href\": \"http://www.filmaffinity.com/es/film136438.html\", \"amount_of_votes\": null, \"formatted_amount_of_votes\": null, \"score\": 6.6, \"highlighted\": false, \"site\": { \"type\": \"sites\", \"id\": \"2\", \"numerical_id\": 2, \"name\": \"FilmAffinity\", \"show_image\": false, \"scale\": 10, \"image\": \"https://images-1.wuaki.tv/system/images/2/original/filmaffinity.png\" } }, { \"type\": \"scores\", \"id\": \"208393\", \"numerical_id\": 208393, \"href\": \"https://www.themoviedb.org/movie/9741\", \"amount_of_votes\": 4904, \"formatted_amount_of_votes\": \"4,9K\", \"score\": 7.1, \"highlighted\": true, \"site\": { \"type\": \"sites\", \"id\": \"40\", \"numerical_id\": 40, \"name\": \"The Movie Database\", \"show_image\": false, \"scale\": 10, \"image\": \"https://images-1.wuaki.tv/system/images/40/original/the-movie-database-1488534910.png\" } }, { \"type\": \"scores\", \"id\": \"256115\", \"numerical_id\": 256115, \"href\": \"http://www.sensacine.com/peliculas/pelicula-27792/\", \"amount_of_votes\": 123, \"formatted_amount_of_votes\": \"123\", \"score\": 7.76, \"highlighted\": false, \"site\": { \"type\": \"sites\", \"id\": \"56\", \"numerical_id\": 56, \"name\": \"Sensacine\", \"show_image\": true, \"scale\": 10, \"image\": \"https://images-3.wuaki.tv/system/images/56/original/sensacine-1555059281.png\" } } ], \"subscription_plans\": [], \"svod_schedules\": [], \"tags\": [], \"view_options\": { \"support\": { \"audio_qualities\": [ { \"type\": \"audio_qualities\", \"id\": \"2.0\", \"numerical_id\": 2, \"name\": \"2.0 (Stereo)\", \"abbr\": \"2.0\", \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/2-0-stereo-1456155689.png\" } ], \"video_qualities\": [ { \"type\": \"video_qualities\", \"id\": \"SD\", \"numerical_id\": 2, \"name\": \"SD\", \"abbr\": \"SD\", \"position\": 0, \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/sd-1456155688.png\" } ], \"hdr_types\": [ { \"type\": \"hdr_types\", \"id\": \"NONE\", \"numerical_id\": 1, \"name\": \"SDR\", \"abbr\": \"NONE\", \"image\": null } ] }, \"public\": { \"trailers\": [ { \"audio_languages\": [ { \"type\": \"languages\", \"id\": \"ENG\", \"numerical_id\": 18, \"name\": \"Ingl\u00E9s\", \"abbr\": \"ENG\" } ], \"subtitle_languages\": [ { \"type\": \"languages\", \"id\": \"MIS\", \"numerical_id\": 1, \"name\": \"Sin subt\u00EDtulos\", \"abbr\": \"MIS\" } ], \"video_qualities\": [ { \"type\": \"video_qualities\", \"id\": \"SD\", \"numerical_id\": 2, \"name\": \"SD\", \"position\": 0, \"abbr\": \"SD\", \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/sd-1456155688.png\" } ], \"audio_qualities\": [ { \"type\": \"audio_qualities\", \"id\": \"2.0\", \"numerical_id\": 2, \"name\": \"2.0 (Stereo)\", \"abbr\": \"2.0\", \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/2-0-stereo-1456155689.png\" } ], \"streaming_drm_types\": [ { \"type\": \"streaming_drm_types\", \"id\": \"PD-NONE\" }, { \"type\": \"streaming_drm_types\", \"id\": \"DASH-NONE\" }, { \"type\": \"streaming_drm_types\", \"id\": \"SS-NONE\" } ], \"hdr_types\": [ { \"type\": \"hdr_types\", \"id\": \"NONE\", \"numerical_id\": 1, \"name\": \"SDR\", \"abbr\": \"NONE\", \"image\": null } ] } ], \"previews\": [], \"adverts\": [] }, \"private\": { \"streams\": [ { \"audio_languages\": [ { \"type\": \"languages\", \"id\": \"ENG\", \"numerical_id\": 18, \"name\": \"Ingl\u00E9s\", \"abbr\": \"ENG\" }, { \"type\": \"languages\", \"id\": \"SPA\", \"numerical_id\": 57, \"name\": \"Espa\u00F1ol\", \"abbr\": \"SPA\" } ], \"subtitle_languages\": [ { \"type\": \"languages\", \"id\": \"MIS\", \"numerical_id\": 1, \"name\": \"Sin subt\u00EDtulos\", \"abbr\": \"MIS\" } ], \"video_qualities\": [ { \"type\": \"video_qualities\", \"id\": \"SD\", \"numerical_id\": 2, \"name\": \"SD\", \"position\": 0, \"abbr\": \"SD\", \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/sd-1456155688.png\" } ], \"audio_qualities\": [ { \"type\": \"audio_qualities\", \"id\": \"2.0\", \"numerical_id\": 2, \"name\": \"2.0 (Stereo)\", \"abbr\": \"2.0\", \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/2-0-stereo-1456155689.png\" } ], \"streaming_drm_types\": [ { \"type\": \"streaming_drm_types\", \"id\": \"PD-WVN\" }, { \"type\": \"streaming_drm_types\", \"id\": \"DASH-CENC\" }, { \"type\": \"streaming_drm_types\", \"id\": \"SS-PR\" } ], \"hdr_types\": [ { \"type\": \"hdr_types\", \"id\": \"NONE\", \"numerical_id\": 1, \"name\": \"SDR\", \"abbr\": \"NONE\", \"image\": null } ] } ], \"offline_streams\": [ { \"audio_languages\": [ { \"type\": \"languages\", \"id\": \"ENG\", \"numerical_id\": 18, \"name\": \"Ingl\u00E9s\", \"abbr\": \"ENG\" }, { \"type\": \"languages\", \"id\": \"SPA\", \"numerical_id\": 57, \"name\": \"Espa\u00F1ol\", \"abbr\": \"SPA\" } ], \"subtitle_languages\": [ { \"type\": \"languages\", \"id\": \"MIS\", \"numerical_id\": 1, \"name\": \"Sin subt\u00EDtulos\", \"abbr\": \"MIS\" } ], \"video_qualities\": [ { \"type\": \"video_qualities\", \"id\": \"SD\", \"numerical_id\": 2, \"name\": \"SD\", \"position\": 0, \"abbr\": \"SD\", \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/sd-1456155688.png\" } ], \"audio_qualities\": [ { \"type\": \"audio_qualities\", \"id\": \"2.0\", \"numerical_id\": 2, \"name\": \"2.0 (Stereo)\", \"abbr\": \"2.0\", \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/2-0-stereo-1456155689.png\" } ], \"streaming_drm_types\": [ { \"type\": \"streaming_drm_types\", \"id\": \"PD-CENC\" } ], \"hdr_types\": [ { \"type\": \"hdr_types\", \"id\": \"NONE\", \"numerical_id\": 1, \"name\": \"SDR\", \"abbr\": \"NONE\", \"image\": null } ] } ] } }, \"additional_images\": {}, \"labels\": { \"audio_qualities\": [ { \"type\": \"audio_qualities\", \"id\": \"2.0\", \"numerical_id\": 2, \"name\": \"2.0 (Stereo)\", \"abbr\": \"2.0\", \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/2-0-stereo-1456155689.png\" } ], \"hdr_types\": [ { \"type\": \"hdr_types\", \"id\": \"NONE\", \"numerical_id\": 1, \"name\": \"SDR\", \"abbr\": \"NONE\", \"image\": null } ], \"purchase_types\": [ { \"type\": \"purchase_types\", \"id\": \"1\", \"numerical_id\": 1, \"is_recurring\": false, \"name\": \"Alquiler 48H\", \"label\": \"ALQUILER 48H\", \"kind\": \"rental\", \"expires_after_in_seconds\": 172800, \"available_time_in_seconds\": 172800 }, { \"type\": \"purchase_types\", \"id\": \"2\", \"numerical_id\": 2, \"is_recurring\": false, \"name\": \"Venta (Digital Locker)\", \"label\": \"VENTA (DIGITAL LOCKER)\", \"kind\": \"purchase\", \"expires_after_in_seconds\": null, \"available_time_in_seconds\": null } ], \"video_qualities\": [ { \"type\": \"video_qualities\", \"id\": \"SD\", \"numerical_id\": 2, \"name\": \"SD\", \"abbr\": \"SD\", \"position\": 0, \"image\": \"https://images-1.wuaki.tv/system/logos/2/original/sd-1456155688.png\" } ] } } }"
    //var detailMovieResponse: DetailMovieResponse = Gson().fromJson(response, DetailMovieResponse::class.java)
    fun createFakeMovieDetailActivityInjector(block: MovieDetailActivity.() -> Unit)
            : DispatchingAndroidInjector<Activity> {

        val injector = AndroidInjector<Activity> { instance ->
            if (instance is MovieDetailActivity) {
                instance.block()
            }
        }

        val factory = AndroidInjector.Factory<Activity> { injector }

        val map = mapOf(
            Pair<Class<out Activity>,
                    Provider<AndroidInjector.Factory<out Activity>>>(
                MovieDetailActivity::class.java,
                Provider { factory })
        )

        return DispatchingAndroidInjector_Factory.newDispatchingAndroidInjector(map)
    }

    @get:Rule
    val activityTestRule = object : ActivityTestRule<MovieDetailActivity>(
        MovieDetailActivity::class.java,
        true,
        // false: do not launch the activity immediately
        false
    ) {

        override fun beforeActivityLaunched() {
            super.beforeActivityLaunched()
            //val myApp = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as MoviesApp
            //myApp.mDispatchingAndroidInjector = createFakeMovieDetailActivityInjector {
            //    mockMovieDetailMapper()
        }


    }

    private fun buildMovieDetailStartIntent(): Intent {
        val context = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext
        return Intent(context, MovieDetailActivity::class.java).apply {
            putExtra(MovieDetailActivity.BUNDLE_MOVIE_ID, buildMockListMovie())
        }
    }


    private fun buildMockListMovie(): MoviesListBindingModel {
        return MoviesListBindingModel(
            "el-protegido",
            "El Protegido",
            "https://images-3.wuaki.tv/system/shots/127408/original/el-protegido-1551183774.jpeg",
            "4.5",
            "2.1"
        )
    }

    @Test
    fun movieTestIsAddedToTheScreen() {
        activityTestRule.launchActivity(buildMovieDetailStartIntent())

        onView(isAssignableFrom(CollapsingToolbarLayout::class.java)
        ).check(
            matches(CollapsingToolbarViewMatcher.withCollapsibleToolbarTitle(Matchers.`is`(TEST_MOVIE_NAME)))
        )

    }


    companion object {

        private val TEST_MOVIE_NAME = "El Protegido"
    }
}