package com.eladiosuarez.rakuten.detailmovie.viewmodel

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesDetailBindingModel
import com.eladiosuarez.rakuten.data.model.bidingModel.MoviesDetailStreamingBindingModel
import com.eladiosuarez.rakuten.data.model.request.streaming.StreamingRequest
import com.eladiosuarez.rakuten.data.model.response.detailmovie.DetailMovieResponse
import com.eladiosuarez.rakuten.data.model.response.detailmovie.Score
import com.eladiosuarez.rakuten.data.model.response.detailmovie.Site
import com.eladiosuarez.rakuten.data.model.response.listmovies.ListMovies
import com.eladiosuarez.rakuten.data.model.response.streaming.DataTrailer
import com.eladiosuarez.rakuten.data.model.response.streaming.Heartbeat
import com.eladiosuarez.rakuten.data.model.response.streaming.StreamingResponse
import com.eladiosuarez.rakuten.data.repository.MovieDataSource
import com.eladiosuarez.rakuten.ui.ActivityNavigator
import com.eladiosuarez.rakuten.ui.detailmovie.viewmodel.MoviesDetailViewModel
import com.nhaarman.mockito_kotlin.spy
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.mockito.Mockito
import org.mockito.Mockito.verify


class MovideDetailViewModelTest {
    inline fun <reified T> lambdaMock(): T = Mockito.mock(T::class.java)

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    var repo: MovieRepoTestDouble = spy(MovieRepoTestDouble())

    lateinit var viewModel: MoviesDetailViewModel

    @Before
    fun setUp() {
        viewModel = MoviesDetailViewModel(
            Mockito.mock(Application::class.java),
            repo,
            Mockito.mock(ActivityNavigator::class.java)
        )
    }

    @org.junit.Test
    fun viewModelTransformationWorks() {
        val observerFunction = lambdaMock<(MoviesDetailBindingModel?) -> Unit>()
        val observer = Observer<MoviesDetailBindingModel> {
            observerFunction.invoke(it)
        }
        viewModel.getMutableMovieData().observeForever(observer)
        verify(repo).getMovieDetail()

    }

    open class MovieRepoTestDouble : MovieDataSource {
        override fun getMovies(id: String): Single<ListMovies> {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun getMovieDetail(idMovie: String): Single<DetailMovieResponse> {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun postTrailerStreaming(streamingRequest: StreamingRequest): Single<StreamingResponse> {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        fun mockMovieDetailMapper(): MoviesDetailBindingModel {
            return MoviesDetailBindingModel(
                builMockTrailer(),
                score = buildMockScoreList(),
                genre = buildListString(),
                synopsis = "Es una pelicula",
                actors = buildListString(),
                title = "Mi peli",
                originalTitle = "Mi película",
                language = buildListString(),
                country = "Spain",
                duration = "180",
                rent = "3.0",
                sell = "17.0",
                age = "12",
                year = "2019",
                poster = "https://images-2.wuaki.tv/system/artworks/4337/master/el-protegido-1558429546.jpeg",
                snapshot = "https://images-3.wuaki.tv/system/shots/127408/original/el-protegido-1551183774.jpeg"
            )
        }

        private fun builMockTrailer(): MoviesDetailStreamingBindingModel {
            return MoviesDetailStreamingBindingModel(
                audioLanguage = "SPA",
                audioQuality = "2.0",
                contentId = "matrix",
                contentType = "movies",
                deviceSerial = "AABBCCDDCC112233",
                deviceStreamVideoQuality = "FHD",
                player = "android:PD-NONE",
                subtitleLanguage = "MIS",
                videoQuality = "HD",
                videoType = "trailer"
            )
        }

        private fun dataTrailer(): DataTrailer {
            return DataTrailer(
                Mockito.mock(Heartbeat::class.java), "", arrayListOf(), ""
            )

        }

        private fun buildMockScoreList(): List<Score> {
            return listOf(
                Score(
                    amountOfVotes = 4904,
                    formattedAmountOfVotes = "4,9K",
                    highlighted = true,
                    href = "https://www.themoviedb.org/movie/9741",
                    id = "208393",
                    numericalId = 208393,
                    score = 7.1,
                    site = Site(
                        "40",
                        "https://images-1.wuaki.tv/system/images/40/original/the-movie-database-1488534910.png",
                        "The Movie Database",
                        40,
                        10,
                        false,
                        type = "sites"
                    ),
                    type = "trailer"
                )
            )
        }

        private fun buildListString(): List<String> {
            return arrayListOf("Test1", "Test2")
        }

        fun getMovieDetail(): LiveData<MoviesDetailBindingModel?> {
            return MutableLiveData<MoviesDetailBindingModel>().apply {
                mockMovieDetailMapper().let {
                    this.value = it
                }
            }
        }

    }
}